This [album](https://album.solutions) catalog is deployed to https://album-app.gitlab.io/catalogs/default.

This catalog is automatically added to album when running it for the first time.