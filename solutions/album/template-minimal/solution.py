from album.runner.api import setup


setup(
    group="album",
    name="template-minimal",
    version="0.1.0",
    album_api_version="0.3.1"
)
